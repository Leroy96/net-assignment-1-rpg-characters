﻿using System;

namespace RPGCharacters
{
    internal class Program
    {
        // Attributes
        Inventory.Inventory inventory = new Inventory.Inventory(4);
        Weapon weapon = new Weapon();
        Armor armor = new Armor();
        PrimaryAttributes totalAttributes = new PrimaryAttributes
        {
            Strength = 0,
            Dexterity = 0,
            Intelligence = 0,
        };

        static void Main(string[] args)
        {
            var p = new Program();
            p.Start();
        }
        public void Start()
        {
            string name;
            string classString;
            bool isNumber = false;

            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine("Hello daring dungeon crawler!!!");
            Console.WriteLine("Are you ready to become the ultimate RPG character?");
            Console.WriteLine("");
            Console.Write("Please enter your name: ");
            name = Console.ReadLine();
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine($"Welcome {name}!");
            Console.WriteLine("");
            Console.WriteLine("  |===== Classes =====|\n" +
                              "  |                   |\n" +
                              "  |    1. Mage        |\n" +
                              "  |    2. Ranger      |\n" +
                              "  |    3. Rogue       |\n" +
                              "  |    4. Warrior     |\n" +
                              "  |___________________|\n");
            while (isNumber == false)
            {
                Console.Write("Choose a class to start your adventure (1-4): ");
                classString = Console.ReadLine();
                Helpers helpers = new Helpers();
                int classNumber = helpers.ConvertStringToNumber(classString);
                if (classNumber > 4 | classNumber < 0)
                {
                    isNumber = false;
                }
                else if (classNumber == 1) // Mage
                {
                    PrimaryAttributes primaryAttributes = new PrimaryAttributes
                    {
                        Strength = 1,
                        Dexterity = 1,
                        Intelligence = 8,
                    };
                    Character character = new Mage(name, 1, primaryAttributes, totalAttributes); // edit last param
                    Actions("Mage", character);
                }
                else if (classNumber == 2) // Ranger
                {
                    PrimaryAttributes primaryAttributes = new PrimaryAttributes
                    {
                        Strength = 1,
                        Dexterity = 7,
                        Intelligence = 1,
                    };
                    Character character = new Ranger(name, 1, primaryAttributes, totalAttributes); // edit last param
                    Actions("Ranger", character);
                }
                else if (classNumber == 3) // Rogue
                {
                    PrimaryAttributes primaryAttributes = new PrimaryAttributes
                    {
                        Strength = 2,
                        Dexterity = 6,
                        Intelligence = 1,
                    };
                    Character character = new Rogue(name, 1, primaryAttributes, totalAttributes); // edit last param
                    Actions("Rogue", character);
                }
                else if (classNumber == 4) // Warrior
                {
                    PrimaryAttributes primaryAttributes = new PrimaryAttributes
                    {
                        Strength = 5,
                        Dexterity = 2,
                        Intelligence = 1,
                    };
                    Character character = new Warrior(name, 1, primaryAttributes, totalAttributes); // edit last param
                    Actions("Warrior", character);
                }
            }
        }

        public void Actions(string type, Character selectedClass)
        {
            Console.WriteLine($"You are now a level {selectedClass.Level} {type}, attack to gain XP\n");
            while (selectedClass != null)
            {
                Console.WriteLine("-------------------------------------------------------------------------\n");
                Console.WriteLine("  |===== Actions =====|\n" +
                                  "  |                   |\n" +
                                  "  |    1. Attack      |\n" +
                                  "  |    2. Stats       |\n" +
                                  "  |    3. Level up    |\n" +
                                  "  |    4. Equipment   |\n" +
                                  "  |    5. End game    |\n" +
                                  "  |___________________|\n");
                Console.Write("Choose an action: ");
                string action = Console.ReadLine();
                int actionValue;
                bool success = int.TryParse(action, out actionValue);
                bool valid = success && 1 <= actionValue && actionValue <= 5;
                while (!valid)
                {
                    Console.WriteLine("Invalid action. Try again...");
                    Console.Write("Choose an action: ");
                    action = Console.ReadLine();
                    success = int.TryParse(action, out actionValue);
                    valid = success && 1 <= actionValue && actionValue <= 5;
                }
                Console.WriteLine("-------------------------------------------------------------------------\n");
                Console.WriteLine($"Your action: {actionValue}\n");
                if (actionValue == 1)
                {
                    selectedClass.IAttack();
                }
                else if (actionValue == 2)
                {
                    selectedClass.IStats();
                    selectedClass.IArmorStats();
                }
                else if (actionValue == 3)
                {
                    selectedClass.LevelupMenu(type, selectedClass);
                }
                else if (actionValue == 4)
                {
                    EquipmentMenu(type, selectedClass);
                }
                else if (actionValue == 5)
                {
                    Console.WriteLine("-------------------------------------------------------------------------\n");
                    Console.WriteLine("Thanks for playing!!!");
                    Environment.Exit(0);
                }
            }
        }

        public void EquipmentMenu(string type, Character selectedClass)
        {
            // Restrictions for classes
            //string[] restrictedArmor;
            //restrictedArmor = new string[2];

            //if (type == "Mage")
            //{
            //    restrictedArmor[0] = "Cloth";
            //}
            //else if (type == "Ranger" || type == "Rogue")
            //{
            //    restrictedArmor[0] = "Leather";
            //    restrictedArmor[1] = "Mail";
            //}
            //else if (type == "Warrior")
            //{
            //    restrictedArmor[0] = "Mail";
            //    restrictedArmor[1] = "Plate";
            //}
            bool EquipmentMenuOpen = true;
            while (EquipmentMenuOpen == true)
            {
                Console.WriteLine("  |========== Equipment ==========|\n" +
                                  "  |                               |\n" +
                                 $"  |    1. Head:     {inventory.GetItem(1)}          |\n" +
                                  "  |                               |\n" +
                                 $"  |    2. Body:     {inventory.GetItem(2)}          |\n" +
                                  "  |                               |\n" +
                                 $"  |    3. Legs:     {inventory.GetItem(3)}          |\n" +
                                  "  |                               |\n" +
                                 $"  |    4. Weapon:   {inventory.GetItem(4)}          |\n" +
                                  "  |                               |\n" +
                                  "  |    5. Back                    |\n" +
                                  "  |_______________________________|\n");
                Console.Write("Choose an action: ");
                string action = Console.ReadLine();
                Helpers helpers = new Helpers();
                int actionNumber = helpers.ConvertStringToNumber(action);
                if (actionNumber == 1)
                {
                    string equipmentType = "Head";
                    armor.IEquipArmor(actionNumber, selectedClass, inventory, equipmentType);
                }
                else if (actionNumber == 2)
                {
                    string equipmentType = "Body";
                    armor.IEquipArmor(actionNumber, selectedClass, inventory, equipmentType);

                }
                else if (actionNumber == 3)
                {
                    string equipmentType = "Legs";
                    armor.IEquipArmor(actionNumber, selectedClass, inventory, equipmentType);
                }
                else if (actionNumber == 4)
                {
                    string equipmentType = "Weapon";
                    weapon.IEquipWeapon(selectedClass, inventory, equipmentType);
                }
                else if (actionNumber == 5)
                {
                    EquipmentMenuOpen = false;
                }
            }
        }
    }
}

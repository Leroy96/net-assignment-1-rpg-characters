﻿using System;

namespace RPGCharacters
{
    public abstract class Character : ICharacter
    {
        public Character()
        {
        }
        public Character(string name, int level, PrimaryAttributes basePrimaryAttributes, PrimaryAttributes totalPrimaryAttributes)
        {
            Name = name;
            Level = level;
            BasePrimaryAttributes = basePrimaryAttributes;
            TotalPrimaryAttributes = totalPrimaryAttributes;
        }

        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }

        /// <summary>
        /// Attack based on attributes
        /// </summary>
        public abstract void IAttack();

        /// <summary>
        /// Display the current stats to the console.
        /// </summary>
        public virtual void IStats()
        {
            Console.WriteLine($"  |===== Player lvl: {Level} =====|\n" +
                               "  |                         |\n" +
                              $"  |    Strength:      {BasePrimaryAttributes.Strength}     |\n" +
                              $"  |    Dexterity:     {BasePrimaryAttributes.Dexterity}     |\n" +
                              $"  |    Intelligence:  {BasePrimaryAttributes.Intelligence}     |\n" +
                               "  |_________________________|\n");
        }

        public virtual void IStats(int level, PrimaryAttributes primaryAttributes)
        {
            Console.WriteLine($"  |===== Player lvl: {level} =====|\n" +
                               "  |                         |\n" +
                              $"  |    Strength:      {primaryAttributes.Strength}     |\n" +
                              $"  |    Dexterity:     {primaryAttributes.Dexterity}     |\n" +
                              $"  |    Intelligence:  {primaryAttributes.Intelligence}     |\n" +
                               "  |_________________________|\n");
        }


        public virtual void IArmorStats()
        {
            Console.WriteLine($"  |===== Armor attributes =====|\n" +
                               "  |                            |\n" +
                              $"  |    Strength:      {TotalPrimaryAttributes.Strength}        |\n" +
                              $"  |    Dexterity:     {TotalPrimaryAttributes.Dexterity}        |\n" +
                              $"  |    Intelligence:  {TotalPrimaryAttributes.Intelligence}        |\n" +
                               "  |____________________________|\n");
        }
        public virtual void IArmorStats(int level, PrimaryAttributes totalAttributes)
        {
            Console.WriteLine($"  |===== Armor attributes =====|\n" +
                               "  |                            |\n" +
                              $"  |    Strength:      {totalAttributes.Strength}        |\n" +
                              $"  |    Dexterity:     {totalAttributes.Dexterity}        |\n" +
                              $"  |    Intelligence:  {totalAttributes.Intelligence}        |\n" +
                               "  |____________________________|\n");
        }

        /// <summary>
        /// Level up menu
        /// </summary>
        public virtual void LevelupMenu(string type, Character selectedClass)
        {
            if (type == "Mage")
            {
                selectedClass.ILevelup(1, 1, 5);
            }
            else if (type == "Ranger")
            {
                selectedClass.ILevelup(1, 5, 1);
            }
            else if (type == "Rogue")
            {
                selectedClass.ILevelup(1, 4, 1);
            }
            else if (type == "Warrior")
            {
                selectedClass.ILevelup(3, 2, 1);
            }
        }
        /// <summary>
        /// Level up (with enough xp).
        /// </summary>
        public virtual void ILevelup(int upStrength, int upDex, int upInt)
        {
            Level = Level + 1;
            PrimaryAttributes primaryAttributes = new PrimaryAttributes
            {
                Strength = BasePrimaryAttributes.Strength + upStrength,
                Dexterity = BasePrimaryAttributes.Dexterity + upDex,
                Intelligence = BasePrimaryAttributes.Intelligence + upInt,
            };
            BasePrimaryAttributes = primaryAttributes;
            IStats(Level, primaryAttributes);
        }

        /// <summary>
        /// If user equips armor, the stats need to be updated.
        /// </summary>
        /// <param name="armorStats"></param>
        public void ITotalStats(PrimaryAttributes armorStats, int slot)
        {
            if (slot == 1)
            {
                PrimaryAttributes headAttributes = new PrimaryAttributes
                {
                    Strength = armorStats.Strength,
                    Dexterity = armorStats.Dexterity,
                    Intelligence = armorStats.Intelligence,
                };
                TotalPrimaryAttributes = TotalPrimaryAttributes + headAttributes;
            }
            if (slot == 2)
            {
                PrimaryAttributes bodyAttributes = new PrimaryAttributes
                {
                    Strength = armorStats.Strength,
                    Dexterity = armorStats.Dexterity,
                    Intelligence = armorStats.Intelligence,
                };
                TotalPrimaryAttributes = TotalPrimaryAttributes + bodyAttributes;
            }
            if (slot == 3)
            {
                PrimaryAttributes legsAttributes = new PrimaryAttributes
                {
                    Strength = armorStats.Strength,
                    Dexterity = armorStats.Dexterity,
                    Intelligence = armorStats.Intelligence,
                };
                TotalPrimaryAttributes = TotalPrimaryAttributes + legsAttributes;
            }
            Console.WriteLine("Armor stats: ");
            IArmorStats(Level, TotalPrimaryAttributes);
        }
    }
}

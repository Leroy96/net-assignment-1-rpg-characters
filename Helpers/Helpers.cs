﻿namespace RPGCharacters
{
    public class Helpers
    {
        public int ConvertStringToNumber(string action)
        {
            int actionNumber;
            bool isNumber = false;
            isNumber = int.TryParse(action, out actionNumber);
            return actionNumber;
        }
    }
}

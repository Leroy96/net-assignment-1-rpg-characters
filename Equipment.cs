﻿using System;
using System.Collections.Generic;

namespace RPGCharacters
{
    public abstract class Equipment : IEquipment
    {
        // Attribute
        Dictionary<string, string> characterEquipment = new Dictionary<string, string>();
        //string head = "none";
        //string body;
        //string legs;
        //string weapon;

        public Equipment()
        {
        }
        public Equipment(string name, int required_Level, Dictionary<string, string> slot)
        {
            Name = name;
            Required_Level = required_Level;
            Slot = slot;
        }

        public string Name { get; set; }
        public int Required_Level { get; set; }
        public Dictionary<string, string> Slot { get; set; }

        public void EquipmentMenu(string type, Character selectedClass)
        {
            if (!characterEquipment.ContainsKey("Head"))
            {
                characterEquipment.Add("Head", "none");
                characterEquipment.Add("Body", "none");
                characterEquipment.Add("Legs", "none");
                characterEquipment.Add("Weapon", "none");
            }


            // Restrictions for classes
            //string[] restrictedArmor;
            //restrictedArmor = new string[2];

            //if (type == "Mage")
            //{
            //    restrictedArmor[0] = "Cloth";
            //}
            //else if (type == "Ranger" || type == "Rogue")
            //{
            //    restrictedArmor[0] = "Leather";
            //    restrictedArmor[1] = "Mail";
            //}
            //else if (type == "Warrior")
            //{
            //    restrictedArmor[0] = "Mail";
            //    restrictedArmor[1] = "Plate";
            //}

            bool EquipmentMenuOpen = true;
            while (EquipmentMenuOpen == true)
            {
                Console.WriteLine("|========== Equipment ==========|\n" +
                                  "|                               |\n" +
                                 $"|    1. Head:     {characterEquipment["Head"]}          |\n" +
                                  "|                               |\n" +
                                 $"|    2. Body:     {characterEquipment["Body"]}          |\n" +
                                  "|                               |\n" +
                                 $"|    3. Legs:     {characterEquipment["Legs"]}          |\n" +
                                  "|                               |\n" +
                                 $"|    4. Weapon:   {characterEquipment["Weapon"]}          |\n" +
                                  "|                               |\n" +
                                  "|    5. Back                    |\n" +
                                  "|_______________________________|\n");
                Console.Write("Choose an action: ");
                string action = Console.ReadLine();
                Helpers helpers = new Helpers();
                int actionNumber = helpers.ConvertStringToNumber(action);
                if (actionNumber == 1)
                {
                    string equipmentType = "Head";
                    string newItem = IEquipArmor(equipmentType, characterEquipment["Head"]);
                    characterEquipment["Head"] = newItem;
                    //head = newItem;
                }
                else if (actionNumber == 2)
                {
                    string equipmentType = "Body";
                    string newItem = IEquipArmor(equipmentType, characterEquipment["Body"]);
                    characterEquipment["Body"] = newItem;
                }
                else if (actionNumber == 3)
                {
                    string equipmentType = "Legs";
                    string newItem = IEquipArmor(equipmentType, characterEquipment["Legs"]);
                    characterEquipment["Legs"] = newItem;
                }
                else if (actionNumber == 4)
                {
                    string equipmentType = "Weapon";
                    string newItem = IEquipWeapon(equipmentType, characterEquipment["Weapon"]);
                    characterEquipment["Weapon"] = newItem;
                }
                else if (actionNumber == 5)
                {
                    EquipmentMenuOpen = false;
                }
            }
        }

        public void IEquipArmor()
        {
            throw new NotImplementedException();
        }
        public string IEquipArmor(string equipmentType, string armorEquipped)
        {
            Console.WriteLine($"|========== {equipmentType} ==========|\n" +
                             "|                          |\n" +
                            $"|   Equipped:   {armorEquipped}     |\n" +
                            $"|                          |\n" +
                            $"|                          |\n" +
                            $"|   Options:               |\n" +
                            $"|    1. Cloth              |\n" +
                            $"|    2. Leather            |\n" +
                            $"|    3. Mail               |\n" +
                            $"|    4. Plate              |\n" +
                             "|    5. Back               |\n" +
                             "|__________________________|\n");
            Console.Write("Choose an option...");
            string option = Console.ReadLine();
            Helpers helpers = new Helpers();
            int optionNumber = helpers.ConvertStringToNumber(option);
            if (optionNumber == 1)
            {
                return "Cloth";
            }
            else if (optionNumber == 2)
            {
                return "Leather";

            }
            else if (optionNumber == 3)
            {
                return "Mail";

            }
            else if (optionNumber == 4)
            {
                return "Plate";
            }
            else if (optionNumber == 5)
            {
                
            }
            return "none";
        }

        public string IEquipWeapon(string equipmentType, string weaponEquipped)
        {
            Console.WriteLine($"|===== {equipmentType} =====|\n" +
                               "|                          |\n" +
                              $"|   Equipped:   {equipmentType}     |\n" +
                              $"|                          |\n" +
                              $"|                          |\n" +
                              $"|   Options:               |\n" +
                              $"|    1. Axe                |\n" +
                              $"|    2. Bow                |\n" +
                              $"|    3. Dagger             |\n" +
                              $"|    4. Hammer             |\n" +
                              $"|    5. Staff              |\n" +
                              $"|    6. Sword              |\n" +
                              $"|    7. Wand               |\n" +
                               "|    8. Back               |\n" +
                               "|__________________________|\n");
            Console.Write("Choose an option...");
            string option = Console.ReadLine();
            Helpers helpers = new Helpers();
            int optionNumber = helpers.ConvertStringToNumber(option);
            if (optionNumber == 1)
            {
                return "Axe";
            }
            else if (optionNumber == 2)
            {
                return "Bow";
            }
            else if (optionNumber == 3)
            {
                return "Dagger";

            }
            else if (optionNumber == 4)
            {
                return "Hammer";
            }
            else if (optionNumber == 5)
            {
                return null;
            }
            return option;
        }

        public void IEquipWeapon()
        {
            throw new NotImplementedException();
        }
    }
}


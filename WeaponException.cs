﻿using System;

namespace RPGCharacters
{
    public class WeaponException : Exception
    {
        public WeaponException(string message) : base(message)
        {
        }

    }

}

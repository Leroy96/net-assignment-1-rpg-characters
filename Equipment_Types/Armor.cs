﻿using System;
using RPGCharacters.Inventory;

namespace RPGCharacters
{
    public class Armor : Item
    {
        Helpers helpers = new Helpers();
        // Enum
        ArmorTypes CLOTH = ArmorTypes.CLOTH;
        ArmorTypes LEATHER = ArmorTypes.LEATHER;
        ArmorTypes MAIL = ArmorTypes.MAIL;
        ArmorTypes PLATE = ArmorTypes.PLATE;

        // Constructor
        public Armor()
        {
        }
        public Armor(string itemName, int itemLevel, int slot, ArmorTypes armorType, PrimaryAttributes attributes) : base(itemName, itemLevel, slot)
        {
            ArmorType = armorType;
            Attributes = attributes;
        }

        public void IEquipArmor(int actionNumber, Character selectedClass, Inventory.Inventory inventory, string equipmentType)
        {
            Console.WriteLine("-------------------------------------------------------------------------\n");
            Console.WriteLine($"  |========== {equipmentType} ==========|\n" +
                               "  |                          |\n" +
                              $"  |   Equipped:   {inventory.GetItem(actionNumber)}     |\n" +
                              $"  |                          |\n" +
                              $"  |                          |\n" +
                              $"  |   Options:               |\n" +
                              $"  |    1. Cloth              |\n" +
                              $"  |    2. Leather            |\n" +
                              $"  |    3. Mail               |\n" +
                              $"  |    4. Plate              |\n" +
                               "  |__________________________|\n");
            Console.Write("Choose an option (1-4): ");
            string option = Console.ReadLine();
            int optionNumber = helpers.ConvertStringToNumber(option);
            if (optionNumber == 1) // Cloth
            {
                SelectedArmor(selectedClass, inventory, actionNumber, "Cloth", 1, CLOTH, 0, 0, 5);
            }
            else if (optionNumber == 2) // Leather
            {
                SelectedArmor(selectedClass, inventory, actionNumber, "Leather", 1, LEATHER, 0, 5, 0);
            }
            else if (optionNumber == 3) // Mail
            {
                SelectedArmor(selectedClass, inventory, actionNumber, "Mail", 1, MAIL, 3, 3, 3);
            }
            else if (optionNumber == 4) // Plate
            {
                SelectedArmor(selectedClass, inventory, actionNumber, "Plate", 1, PLATE, 5, 0, 0);
            }
        }

        public void SelectedArmor(Character selectedClass, Inventory.Inventory inventory, int slot, string itemName, int itemLevel, ArmorTypes armorType, int str, int dex, int intel)
        {
            Armor armor = new Armor()
            {
                ItemName = itemName,
                ItemLevel = itemLevel,
                Slot = slot,
                ArmorType = armorType,
                Attributes = new PrimaryAttributes()
                {
                    Strength = str,
                    Dexterity = dex,
                    Intelligence = intel,
                }
            };
            inventory.AddArmor(armor);
            selectedClass.ITotalStats(armor.Attributes, slot);
        }
    }
}

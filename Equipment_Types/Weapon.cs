﻿using System;
using RPGCharacters.Inventory;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        Helpers helpers = new Helpers();
        float dps = 1;
        // Enum
        WeaponTypes AXE = WeaponTypes.AXE;
        WeaponTypes BOW = WeaponTypes.BOW;
        WeaponTypes DAGGER = WeaponTypes.DAGGER;
        WeaponTypes HAMMER = WeaponTypes.HAMMER;
        WeaponTypes STAFF = WeaponTypes.STAFF;
        WeaponTypes SWORD = WeaponTypes.SWORD;
        WeaponTypes WAND = WeaponTypes.WAND;

        // Constructor
        public Weapon()
        {
        }
        public Weapon(string itemName, int itemLevel, int slot, WeaponTypes weaponType, WeaponAttributes weaponStats) : base(itemName, itemLevel, slot)
        {
            WeaponType = weaponType;
            WeaponAttributes = weaponStats;
        }

        public void IEquipWeapon(Character selectedClass, Inventory.Inventory inventory, string equipmentType)
        {
            Console.WriteLine("-------------------------------------------------------------------------\n");
            Console.WriteLine($"  |===== {equipmentType} =====|\n" +
                               "  |                          |\n" +
                              $"  |   Equipped:   {inventory.GetItem(4)}     |\n" +
                              $"  |                          |\n" +
                              $"  |                          |\n" +
                              $"  |   Options:               |\n" +
                              $"  |    1. Axe                |\n" +
                              $"  |    2. Bow                |\n" +
                              $"  |    3. Dagger             |\n" +
                              $"  |    4. Hammer             |\n" +
                              $"  |    5. Staff              |\n" +
                              $"  |    6. Sword              |\n" +
                              $"  |    7. Wand               |\n" +
                               "  |__________________________|\n");
            Console.Write("Choose an option (1-7): ");
            string option = Console.ReadLine();
            int optionNumber = helpers.ConvertStringToNumber(option);
            try
            {
                if (optionNumber == 1) // Axe
                {
                    SelectedWeapon(selectedClass, inventory, "Great Axe", 1, AXE, 12, 0.8);
                }
                else if (optionNumber == 2) // Bow
                {
                    SelectedWeapon(selectedClass, inventory, "Hunter Bow", 1, BOW, 10, 1.0);
                }
                else if (optionNumber == 3) // Dagger
                {
                    SelectedWeapon(selectedClass, inventory, "Pointy Dagger", 1, DAGGER, 4, 2.5);
                }
                else if (optionNumber == 4) // Hammer
                {
                    SelectedWeapon(selectedClass, inventory, "Blunt Hammer", 2, HAMMER, 12, 0.8);
                }
                else if (optionNumber == 5) // Staff
                {
                    SelectedWeapon(selectedClass, inventory, "Fire Staff", 3, STAFF, 7, 1.5);
                }
                else if (optionNumber == 6) // Sword
                {
                    SelectedWeapon(selectedClass, inventory, "Common Sword", 1, SWORD, 5, 2.0);
                }
                else if (optionNumber == 7) // Wand
                {
                    SelectedWeapon(selectedClass, inventory, "Magic Wand", 1, WAND, 7, 1.5);
                }
            }
            catch (WeaponException ex)
            {
                Console.WriteLine("Weapon exception: " + ex.Message);
            }
        }

        public void SelectedWeapon(Character selectedClass, Inventory.Inventory inventory, string itemName, int itemLevel, WeaponTypes weaponType, int damage, double attackSpeed)
        {
            Weapon weapon = new Weapon() { ItemName = itemName, ItemLevel = itemLevel, Slot = 4, WeaponType = weaponType, WeaponAttributes = new WeaponAttributes() { Damage = damage, AttackSpeed = attackSpeed } };

            if (weapon.ItemLevel > selectedClass.Level)
            {
                throw new WeaponException("Your level is too low to equip this weapon.\n");
            }
            else
            {
                dps = (float)(weapon.WeaponAttributes.Damage * weapon.WeaponAttributes.AttackSpeed);
                inventory.AddWeapon(weapon);
            }
        }

        public double CharacterDamage()
        {
            return dps;
        }
    }
}
﻿using System;

namespace RPGCharacters
{
    public class Mage : Character
    {
        public Mage()
        {
        }
        public Mage(string name, int level, PrimaryAttributes basePrimaryAttributes, PrimaryAttributes totalPrimaryAttributes) : base(name, level, basePrimaryAttributes, totalPrimaryAttributes)
        {
        }

        public override void IAttack()
        {
            Weapon weapon = new Weapon();
            float dps = (float)weapon.CharacterDamage();
            Console.WriteLine(BasePrimaryAttributes.Intelligence);
            Console.WriteLine(TotalPrimaryAttributes.Intelligence);
            Console.WriteLine(dps);
            float damage = dps * (1 + (BasePrimaryAttributes.Intelligence + TotalPrimaryAttributes.Intelligence) / 100);
            Console.WriteLine(damage);

            Console.WriteLine($"{Name} deals {damage} *magic* damage\n");

        }

    }
}

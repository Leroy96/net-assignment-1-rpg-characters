﻿using System;

namespace RPGCharacters
{
    public class Rogue : Character
    {
        public Rogue()
        {

        }
        public Rogue(string name, int level, PrimaryAttributes basePrimaryAttributes, PrimaryAttributes totalPrimaryAttributes) : base(name, level, basePrimaryAttributes, totalPrimaryAttributes)
        {
        }

        public override void IAttack()
        {
            Weapon weapon = new Weapon();
            float dps = (float)weapon.CharacterDamage();
            Console.WriteLine(BasePrimaryAttributes.Dexterity);
            Console.WriteLine(TotalPrimaryAttributes.Dexterity);
            Console.WriteLine(dps);
            float damage = dps * (1 + (BasePrimaryAttributes.Dexterity + TotalPrimaryAttributes.Dexterity) / 100);
            Console.WriteLine(damage);
            Console.WriteLine($"{Name} deals {damage} *stealth* damage\n");
        }
    }
}

﻿using System;

namespace RPGCharacters
{
    public class Warrior : Character
    {
        public Warrior()
        {

        }
        public Warrior(string name, int level, PrimaryAttributes basePrimaryAttributes, PrimaryAttributes totalPrimaryAttributes) : base(name, level, basePrimaryAttributes, totalPrimaryAttributes)
        {
        }

        public override void IAttack()
        {
            Weapon weapon = new Weapon();
            float dps = (float)weapon.CharacterDamage();
            Console.WriteLine(BasePrimaryAttributes.Strength);
            Console.WriteLine(TotalPrimaryAttributes.Strength);
            Console.WriteLine(dps);
            float damage = dps * (1 + (BasePrimaryAttributes.Strength + TotalPrimaryAttributes.Strength) / 100);
            Console.WriteLine(damage);
            Console.WriteLine($"{Name} deals {damage} *slash* damage\n");
        }
    }
}

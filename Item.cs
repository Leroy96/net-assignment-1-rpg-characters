﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Item : Equipment
    {
        public Item(string name, int required_Level, int slot)
        {
            Name = name;
            Required_Level = required_Level;
            Slot = slot;
        }

        public string Name { get; set; }
        public int Required_Level { get; set; }
        public int Slot { get; set; }
        /*
         * Slot 1 = weapon => slot[0]
         *Slot 2 = armor  => slot [1]
         */



        /// <summary>
        /// Equip weapon that the character can equip.
        /// </summary>
        public abstract void IEquip();

    }
}

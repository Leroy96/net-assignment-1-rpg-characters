﻿using System;

namespace RPGCharacters.Inventory
{
    public class InventoryException : Exception
    {
        public InventoryException(string message) : base(message)
        {
        }

        public override string Message => "Inventory exception";
    }
}

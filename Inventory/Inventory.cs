﻿using System;
using System.Collections.Generic;

namespace RPGCharacters.Inventory
{
    public class Inventory
    {
        private Dictionary<int, Item> ItemsInventory { get; set; }
        public Inventory(int size)
        {
            if (size < 1)
            {
                throw new InventoryException("Can't initialize an inventory with size less than 1.");
            }
            InitializeInventory(size);
        }

        private void InitializeInventory(int size)
        {
            ItemsInventory = new Dictionary<int, Item>();
            for (int i = 1; i <= size; i++)
            {
                ItemsInventory.Add(i, null);
            }
        }

        public int GetInvertorySize()
        {
            // Count elements and return value
            return ItemsInventory.Count;
        }

        public void CheckInventory()
        {
            foreach (KeyValuePair<int, Item> item in ItemsInventory)
            {
                if (item.Value != null)
                {
                    Console.WriteLine("Key = {0}, Value = {1}",
                        item.Key, item.Value.ItemName);
                }
            }
        }

        public int AddItem(Item itemToAdd)
        {
            // check first slot where item == null
            foreach (KeyValuePair<int, Item> item in ItemsInventory)
            {
                if (item.Value == null)
                {
                    ItemsInventory[item.Key] = itemToAdd;
                    return item.Key;
                }
            }
            throw new InventoryException("Inventory full");
        }

        public void AddWeapon(Weapon weaponToAdd)
        {
            ItemsInventory[weaponToAdd.Slot] = weaponToAdd;
            Console.WriteLine($"Added weapon: {weaponToAdd.ItemName}\n");
        }

        public void AddArmor(Armor armorToAdd)
        {
            ItemsInventory[armorToAdd.Slot] = armorToAdd;
            Console.WriteLine($"Added armor: {armorToAdd.ItemName}\n");
        }


        public string GetItem(int slot)
        {
            Item equippedWeapon = ItemsInventory[slot];
            if (equippedWeapon == null)
            {
                return "none";
            }
            return equippedWeapon.ItemName;
        }

        public Item RemoveItem(int slot)
        {
            if (slot < 1 || slot > ItemsInventory.Count)
                throw new InventoryException("Must select a valid slot");
            if (ItemsInventory[slot] == null)
                throw new InventoryException("No item in that slot");
            Item removedItem = ItemsInventory[slot];
            ItemsInventory[slot] = null;
            return removedItem;
        }
    }
}

﻿namespace RPGCharacters.Inventory
{
    public class Item
    {
        public Item()
        {
        }
        public Item(string itemName, int itemLevel, int slot)
        {
            ItemName = itemName;
            ItemLevel = itemLevel;
            Slot = slot;
        }

        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public int Slot { get; set; }
        public WeaponTypes WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }
        public ArmorTypes ArmorType { get; set; }
        public PrimaryAttributes Attributes { get; set; }
    }
}

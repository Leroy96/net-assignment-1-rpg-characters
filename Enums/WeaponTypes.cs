﻿namespace RPGCharacters
{
    public enum WeaponTypes
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    // Mages -> Staff, Wand
    // Rangers -> Bows
    // Rogue -> Daggers, Sword
    // Warrior -> Axe, Hammer, Sword
}

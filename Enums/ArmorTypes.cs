﻿namespace RPGCharacters
{
    public enum ArmorTypes
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    // Mages -> Cloth
    // Rangers -> Leather, Mail
    // Rogue -> Leather, Mail
    // Warrior -> Mail, Plate
}

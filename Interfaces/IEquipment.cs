﻿namespace RPGCharacters
{
    public interface IEquipment
    {
        public void IEquipArmor();
        public void IEquipWeapon();
    }
}
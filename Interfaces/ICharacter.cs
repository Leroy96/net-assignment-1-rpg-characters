﻿namespace RPGCharacters
{
    public interface ICharacter
    {
        public void IAttack();
        public void ILevelup(int upStrength, int upDex, int upInt);
        public void IStats();
    }
}
